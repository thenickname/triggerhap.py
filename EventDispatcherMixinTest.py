import unittest
from EventDispatcherMixin import EventDispatcherMixin


class EventDispatcherMixinTest( unittest.TestCase ) :

  _event_name = "my_event"

  def setUp( self ) :
    class Subclass( EventDispatcherMixin ) :
      pass
    self._dispatcher = Subclass()

  def tearDown( self ) :
    del self._dispatcher

  def test_listeners_var_name_is_mangled_to_prevent_accidental_override( self ) :
    self.assertTrue( hasattr( self._dispatcher, '_EventDispatcherMixin__listeners' ) )
    self.assertFalse( hasattr( self._dispatcher, '__listeners' ) )

  def test_on( self ) :
    expected_1 = self._register_listener( self._event_name )
    expected_2 = self._register_listener( self._event_name )
    actual_1 = self._dispatcher._EventDispatcherMixin__listeners[ self._event_name ][ 0 ]
    actual_2 = self._dispatcher._EventDispatcherMixin__listeners[ self._event_name ][ 1 ]
    self.assertEqual( actual_1, expected_1 )
    self.assertEqual( actual_2, expected_2 )

  def test_off_all_listeners( self ) :
    self._register_listener( self._event_name )
    self._dispatcher.off()
    self.assertTrue( len( self._dispatcher._EventDispatcherMixin__listeners.keys() ) == 0 )

  def test_off_all_listeners_for_one_event( self ) :
    event_name_2 = "my_event_2"
    self._register_listener( self._event_name )
    self._register_listener( event_name_2 )
    self._dispatcher.off( event_name_2 )
    self.assertFalse( event_name_2 in self._dispatcher._EventDispatcherMixin__listeners )
    self.assertTrue( self._event_name in self._dispatcher._EventDispatcherMixin__listeners )

  def test_off_all_listeners_for_one_event__unregistered_event( self ) :
    self._dispatcher.off( self._event_name )

  def test_off_one_listener_for_one_event( self ) :
    event_name_2 = "my_event_2"
    def listener() :
      pass
    self._dispatcher.on( self._event_name, listener )
    self._dispatcher.on( self._event_name, listener )
    self._dispatcher.on( event_name_2, listener )
    self._dispatcher.off( self._event_name, listener )
    self.assertTrue( listener in
                     self._dispatcher._EventDispatcherMixin__listeners[ event_name_2 ] )
    self.assertTrue( listener not in
                     self._dispatcher._EventDispatcherMixin__listeners[ self._event_name ] )

  def test_off_one_listener_for_one_event__unregistered_event( self ) :
    def listener() :
      pass
    self._dispatcher.off( self._event_name, listener )

  def test_off_one_listener_for_all_events( self ) :
    def listener_1() :
      pass
    def listener_2() :
      pass
    event_name_1 = "my_event_1"
    event_name_2 = "my_event_2"
    self._dispatcher.on( event_name_1, listener_1 )
    self._dispatcher.on( event_name_2, listener_1 )
    self._dispatcher.on( event_name_2, listener_2 )
    self._dispatcher.off( listener = listener_1 )
    self.assertTrue( listener_1 not in
                     self._dispatcher._EventDispatcherMixin__listeners[ event_name_1 ] )
    self.assertTrue( listener_1 not in
                     self._dispatcher._EventDispatcherMixin__listeners[ event_name_2 ] )
    self.assertTrue( listener_2 in
                     self._dispatcher._EventDispatcherMixin__listeners[ event_name_2 ] )

  def test_off_nonexistent_listener( self ) :
    self._register_listener( self._event_name )
    def listener_2() :
      pass
    self._dispatcher.off( self._event_name, listener_2 )
  
  def test_off_does_not_treat_falsy_params_as_nonexistent( self ) :
    self._register_listener( "" )
    self._register_listener( 0 )
    self._dispatcher.off( "" )
    self.assertFalse( "" in self._dispatcher._EventDispatcherMixin__listeners )
    self.assertTrue( 0 in self._dispatcher._EventDispatcherMixin__listeners )

  def test_trigger( self ) :
    i = 0
    def listener() :
      nonlocal i
      i += 1
    self._dispatcher.on( self._event_name, listener )
    self._dispatcher.on( self._event_name, listener )
    self._dispatcher.on( self._event_name, listener )
    self._dispatcher.trigger( self._event_name )
    self.assertEqual( i, 3 )

  def test_trigger_with_passed_data( self ) :
    expected_data = [ 1, 2, 3 ]
    actual_data = None
    def listener( passed_data ) :
      nonlocal actual_data
      actual_data = passed_data
    self._dispatcher.on( self._event_name, listener )
    self._dispatcher.trigger( self._event_name, expected_data )
    self.assertEqual( actual_data, expected_data )
    
  def test_trigger_does_not_treat_falsy_event_data_as_nonexistent( self ) :
    expected_data = ""
    actual_data = None
    def listener( event_data = None ) :
      nonlocal actual_data
      actual_data = event_data
    self._dispatcher.on( self._event_name, listener )
    self._dispatcher.trigger( self._event_name, expected_data )
    self.assertEqual( actual_data, expected_data )

  def test_trigger__unregistered_event( self ) :
    self._dispatcher.trigger( self._event_name )
  
  def _register_listener( self, event ) :
    def func() :
      pass
    self._dispatcher.on( event, func )
    return func

