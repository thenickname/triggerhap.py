# README #

### triggerhap.py ###

Provides a simple, generic EventDispatcher implementation for Python with a jQuery-like API for registering and unregistering listeners and for triggering events.

This mixin allows you to:

* listen for and trigger arbitrary events using Python functions as listeners/callbacks
* pass generic event data to listeners
* use any hashable Python object as an event
* remove specific listeners or remove all listeners



### Example usage ###

```
#!python

from EventDispatcherMixin import EventDispatcherMixin


class Foo( EventDispatcherMixin ) :

  def __init__( self ) :
    super().__init__()

  def bar( self ) :
    self.trigger( "bar_event" )

  def bar_with_passed_data( self ) :
    self.trigger( "bar_event", [ 1, 2, 3 ] )




# define a listener
def on_bar_listener( event_data = None ) :
  print( "bar happened!", event_data )


# create object that will trigger events
foo = Foo()


# register listener
foo.on( "bar_event", on_bar_listener )


# something in foo triggers bar_events
foo.bar()
foo.bar_with_passed_data()


# stop listening for bar_events
foo.off( "bar_event" )
foo.bar()


# use something different than a string as event name
event = ( 10, 20, 30 )
foo.on( event, on_bar_listener )
foo.trigger( event, 42 )


# register multiple listeners
def another_listener( event_data = None ) :
  print( "another listener", event_data )

foo.on( event, another_listener )
foo.trigger( event, { "event_data" : 42 } )


# unregister specific listeners
foo.off( listener = another_listener )
foo.trigger( event )



```

run unit test using

```
#!bash

python -m unittest -v EventDispatcherMixinTest.py
```
