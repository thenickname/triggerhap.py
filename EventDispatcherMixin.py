class EventDispatcherMixin :
    
  def __init__( self ) :
    self.__listeners = {}

  def on( self, event_name, listener ) :
    if event_name not in self.__listeners :
      self.__listeners[ event_name ] = []
    self.__listeners[ event_name ].append( listener )

  def off( self, event_name = None, listener = None ) :
    try :
      if event_name is None and listener is None :
        self.__listeners = {}
      elif event_name is not None and listener is None :
        del self.__listeners[ event_name ]
      elif event_name is None and listener is not None :
        for event_name in self.__listeners :
          self.__listeners[ event_name ] = self._remove_listener( event_name, listener )
      else :
        self.__listeners[ event_name ] = self._remove_listener( event_name, listener )
    except KeyError :
      pass
    
  def trigger( self, event_name, event_data = None ) :
    try :
      for listener in self.__listeners[ event_name ] :
        if event_data is not None :
          listener( event_data )
        else :
          listener()
    except KeyError :
      pass
  
  def _remove_listener( self, event_name, listener ) :
    return [ func for func in self.__listeners[ event_name ] if func != listener ]

